
public class TestInterfaces {
    public static void main(String[] args) {

        Detailable[] test = new Detailable[3];

        test[0] = new CurrentAccount(200, "Teddy");
        test[1] = new SavingsAccount(400, "Robert");
        test[2] = new HomeInsurance(30, 400, 200);

        for(int i = 0; i < 3; i++){
            System.out.println(test[i].getDetails());
        }
    }    
}
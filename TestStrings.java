public class TestStrings {

    public static void main(String[] args) {
        //question 1
        System.out.println("--------Question 1--------");
        String x = "example.doc";
        String y = "bak";
        String z = x.substring(0, 8) + y;
        System.out.println(z);

        //question 2
        System.out.println("--------Question 2--------");
        String test1 = "Hello";
        String test2 = "World";
        String test3 = "Hello";

        //compare 1
        int temp = test1.compareTo(test2);
        if(temp == 0){
            System.out.println("They are the same!");
        } else if(temp > 0){
            System.out.println(test1 + " is lexicographically  further");
        } else {
            System.out.println(test2 + " is lexicographically  further");
        }

        //compare 2
        int temp2 = test1.compareTo(test3);
        if(temp2 == 0){
            System.out.println("They are the same!");
        } else if(temp2 > 0){
            System.out.println(test1 + " is lexicographically  further");
        } else {
            System.out.println(test3 + " is lexicographically  further");
        }

        //question 3
        System.out.println("--------Question 3--------");
        String test4 = "the quick brown fox swallowed down the lazy chicken";
        String sample = "ow";
        int lastIndex = 0;
        int count = 0;

        while(lastIndex != -1){

            lastIndex = test4.indexOf(sample,lastIndex);
        
            if(lastIndex != -1){
                count ++;
                lastIndex += sample.length();
            }
        }
        System.out.println("It occurs: " + count + " times");

        //question 5
        System.out.println("--------Question 5--------");
        String paliTest = "abba";
        int l = paliTest.length();
        boolean palidrome = true;

        for(int i = 0; i < l/2; i++){
            if(paliTest.charAt(i) != paliTest.charAt(l-1-i)){
                System.out.println("Not a palidrome");
                palidrome = false;
            }
        }

        if(palidrome) {
                System.out.println("Is a palidrome");
        }
    }
    
}
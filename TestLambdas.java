public class TestLambdas {
    public static void main(String[] args) {
        Detailable det = () -> {return "hello world";};
        Detailable det2 = () -> {return "Hi World";};


        System.out.println(det.getDetails());
        System.out.println(det2.getDetails());

        det = () -> {return "Bye Bye";};
        System.out.println(det.getDetails());
    }
}
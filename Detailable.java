public interface Detailable {

    abstract public String getDetails();
    
}
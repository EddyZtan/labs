import java.util.HashSet;

public class CollectionsTest {

    public static void main(String[] args) {
        
        HashSet<Account> accounts;

        accounts = new HashSet<Account>();

        SavingsAccount acc1 = new SavingsAccount(200, "Tim");
        SavingsAccount acc2 = new SavingsAccount(150, "Ryan");
        SavingsAccount acc3 = new SavingsAccount(500, "Elise");

        accounts.add(acc1);
        accounts.add(acc2);
        accounts.add(acc3);

        for (Account account : accounts) {
            System.out.println("Account name: " + account.getName() + " account balance: " + account.getBalance());
        }
    }
    
}
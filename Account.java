public abstract class Account implements Detailable{

    //properties

    private double balance;
    private String name;

    static double interestRate; 

    //get and set

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public double getBalance() {
        return balance;
    }

    public String getName() {
        return name;
    }

    //constructor 
    public Account(double balance, String name){
        this.balance = balance;
        this.name = name;
    }

    //overloaded withDraw
    public boolean withDraw(double amount) {

        if(this.balance < amount){
            return false;
        } else {
            this.balance-=amount; 
        }
        return true;
    }

    public boolean withDraw(){
        
        if(this.balance < 20){
            return false;
        } else {
            this.balance-=20;
        }

        return true;
    }

    //interest rates
    static public void setInterestRate(double rate){
        interestRate = rate; 
    }

    public abstract void addInterest();

    /*
    public void addInterest(){
        this.balance = this.balance * (1 + interestRate); 
    }*/

    //implement Detailable
    @Override
    public String getDetails() {
        
        return "" + name + " " + balance;
        
    }
}
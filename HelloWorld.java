public class HelloWorld {

    public static void main(String[] args) {
        
        String make = "Hyundai";
        String model = "Elantra";
        double engineSize = 1.0;
        byte gear = 3; 
        short speed = (short)(gear * 20);

        System.out.println("--------Car example--------");
        System.out.println("The make is " + make);
        System.out.println("The model is " + model);
        System.out.println("The engine size is " + engineSize);
        System.out.println("The gear is " + gear);
        System.out.println("The speed is " + speed);


        if(gear > 1.3){
                System.out.println("Powerful car");
        } else {
                System.out.println("weak car");
        }

        System.out.println("--------Calendar example--------");
        int[] leapYears = new int[10];
        int year = 1900;

        for(int i = 0; i < 10; i++){
            if(year != 1900){
                if((year % 4) == 0){
                    leapYears[i] = year; 
                    System.out.println(leapYears[i]);
                } else {
                    i-=1;
                }
            } else {
                i-=1;
            }
            year+=1; 
        }

        System.out.println("--------Account example--------");
    }

}
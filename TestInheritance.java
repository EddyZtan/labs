public class TestInheritance {

    public static void main(String[] args) {
        
        Account[] myAcc = {new SavingsAccount(2, "Edward"), new SavingsAccount(4, "Ryan"), new CurrentAccount(6, "San")};

        for(int i = 0; i < myAcc.length; i++){
            myAcc[i].addInterest();
            System.out.println("The account of: " + myAcc[i].getName() + " now has: " + myAcc[i].getBalance());
        }

    }
    
}